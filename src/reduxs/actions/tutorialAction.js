import {
  add_tutorial,
  delete_tutorial_by_id,
  fetch_all_tutorials,
} from "../../services/tutorials.service";
import Swal from "sweetalert2";
export const FETCH_ALL_TUTORIALS = "FETCH_ALL_TUTORIALS";
export const DELETE_TUTORIAL_BY_ID = "DELETE_TUTORIAL_BY_ID";
export const ADD_TUTORIAL = "ADD_TUTORIAL";

export const fetchAllTutorials = () => {
  return async (dispatch) => {
    const result = await fetch_all_tutorials();
    dispatch({
      type: FETCH_ALL_TUTORIALS,
      payload: result,
    });
  };
};

export const addTutorial = (tutorial) => {
  return async (dispatch) => {
    //   const result = await add_tutorial(tutorial);
    const result = await add_tutorial(tutorial);
    dispatch({
      type: ADD_TUTORIAL,
      payload: result,
    });
  };
};

export const deleteTutorialById = (id) => {
  return async (dispatch) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const result = await delete_tutorial_by_id(id);
        dispatch({
          type: DELETE_TUTORIAL_BY_ID,
          payload: id,
        });
        Swal.fire("Deleted!", result.data.message, "success");
      }
    });
  };
};
