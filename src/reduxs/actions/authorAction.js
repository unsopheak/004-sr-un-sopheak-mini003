import {
  add_author,
  delete_author_by_id,
  fetch_all_authors,
} from "../../services/authors.service";
import Swal from "sweetalert2";
export const FETCH_ALL_AUTHORS = "FETCH_ALL_AUTHORS";
export const DELETE_AUTHPOR_BY_ID = "DELETE_AUTHOR_BY_ID";
export const ADD_AUTHOR = "ADD_AUTHOR";

export const fetchAllAuthors = () => {
  return async (dispatch) => {
    const result = await fetch_all_authors();
    dispatch({
      type: FETCH_ALL_AUTHORS,
      payload: result,
    });
  };
};

export const addAuthor = (author) => {
  return async (dispatch) => {
    //   const result = await add_tutorial(tutorial);
    const result = await add_author(author);
    dispatch({
      type: ADD_AUTHOR,
      payload: result,
    });
  };
};

export const deleteAuthorById = (id) => {
  return async (dispatch) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const result = await delete_author_by_id(id);
        dispatch({
          type: DELETE_AUTHPOR_BY_ID,
          payload: id,
        });
        Swal.fire("Deleted!", result.data.message, "success");
      }
    });
  };
};
