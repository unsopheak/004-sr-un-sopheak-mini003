
import Swal from "sweetalert2";
import { add_category, delete_category_by_id, fetch_all_categorys } from "../../services/categorys.service";
export const FETCH_ALL_CATEGORYS = "FETCH_ALL_CATEGORYS";
export const DELETE_CATEGORY_BY_ID = "DELETE_CATEGORY_BY_ID";
export const ADD_CATEGORY = "ADD_CATEGORY";

export const fetchAllCategory = () => {
  return async (dispatch) => {
    const result = await fetch_all_categorys();
    dispatch({
      type: FETCH_ALL_CATEGORYS,
      payload: result,
    });
  };
};

export const addCategory = (category) => {
  return async (dispatch) => {
    const result = await add_category(category);
    dispatch({
      type: ADD_CATEGORY,
      payload: result,
    });
  };
};

export const deleteCategoryById = (id) => {
  return async (dispatch) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const result = await delete_category_by_id(id);
        dispatch({
          type: DELETE_CATEGORY_BY_ID,
          payload: id,
        });
        Swal.fire("Deleted!", result.data.message, "success");
      }
    });
  };
};
