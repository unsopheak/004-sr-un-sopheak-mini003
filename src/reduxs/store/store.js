import {applyMiddleware, combineReducers, createStore} from "redux"
import thunk from "redux-thunk"
import tutorialReducer from "../reducers/tutorialReducer"
import authorReducer from "../reducers/authorReducer"
import categoryReducer from "../reducers/categoryReducer"
 const rootReducer = combineReducers({
     tutorialReducer,
     authorReducer,
     categoryReducer
 })

 
export const store=createStore(rootReducer,applyMiddleware(thunk))