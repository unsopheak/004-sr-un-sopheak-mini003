import { ADD_CATEGORY, DELETE_CATEGORY_BY_ID, FETCH_ALL_CATEGORYS } from "../actions/categoryAction";


const initState = {
  category: [],
};

export default function categoryReducer(state = initState, { type, payload }) {

  switch (type) {
    case FETCH_ALL_CATEGORYS:
      return {
        ...state,
        category: payload,
      };

    case DELETE_CATEGORY_BY_ID:
      return {
        ...state,
        category: state.category.filter((item) => item._id != payload),
      };

    case ADD_CATEGORY:
      return {
        ...state,
        category: [...state.category, payload],
      };
    default:
      return state;
  }
}
