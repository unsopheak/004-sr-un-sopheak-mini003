import {
  FETCH_ALL_TUTORIALS,
  DELETE_TUTORIAL_BY_ID,
  ADD_TUTORIAL,
} from "../actions/tutorialAction";

const initState = {
  tutorials: [],
};

export default function tutorialReducer(state = initState, { type, payload }) {
 
  switch (type) {
    case FETCH_ALL_TUTORIALS:
      return {
        ...state,
        tutorials: payload,
      };

    case DELETE_TUTORIAL_BY_ID:
      return {
        ...state,
        tutorials: state.tutorials.filter((item) => item._id != payload),
      };

    case ADD_TUTORIAL:
      return {
        ...state,
        tutorials: [...state.tutorials, payload],
      };
    default:
      return state;
  }
}
