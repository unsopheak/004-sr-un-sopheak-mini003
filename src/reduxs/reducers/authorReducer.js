import { ADD_AUTHOR, FETCH_ALL_AUTHORS } from "../actions/authorAction";
import { DELETE_TUTORIAL_BY_ID } from "../actions/tutorialAction";

const initState = {
  author: [],
};

export default function authorReducer(state = initState, { type, payload }) {
 
  switch (type) {
    case FETCH_ALL_AUTHORS:
      return {
        ...state,
        author: payload,
      };

    case DELETE_TUTORIAL_BY_ID:
      return {
        ...state,
        author: state.author.filter((item) => item._id != payload),
      };

    case ADD_AUTHOR:
      return {
        ...state,
        author: [...state.author, payload],
      };
    default:
      return state;
  }
}
