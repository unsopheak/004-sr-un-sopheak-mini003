import React from "react";
import { Navbar, Button, NavDropdown, Nav, Container,Form,FormControl } from "react-bootstrap";
import { NavLink} from "react-router-dom";
export default function Menu() {
  return (
    <Navbar collapseOnSelect expand="lg" className="navbar" variant="dark">
      <Container>
        <Navbar.Brand href="#home">AMS Redux</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/article">
              Article
            </Nav.Link>
            <Nav.Link as={NavLink} to="/author">
              Author
            </Nav.Link>
            <Nav.Link as={NavLink} to="/category">
              Category
            </Nav.Link>
            <NavDropdown title="Language" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Khmer</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                English
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Form className="d-flex">
      <FormControl
        type="search"
        placeholder="Search"
        className="mr-2"
        aria-label="Search"
      />
      <Button variant="outline-success">Search</Button>
    </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
