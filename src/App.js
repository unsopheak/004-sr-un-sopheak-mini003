import "./App.css";
import Home from "./views/Home";
import Article from "./views/Article";
import Author from "./views/Author";
import Category from "./views/Category";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Menu from "./components/Menu";
import { Container } from "react-bootstrap";
import LoginWithFacebook from "./views/LoginWithFacebook";
import "bootstrap/dist/css/bootstrap.min.css";
function App() {
  return (
    <BrowserRouter>
      <Menu />
      <Container>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/article" component={Article} />
          <Route path="/author" component={Author} />
          <Route path="/category" component={Category} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
