import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FacebookLogin from "react-facebook-login";
import {
  addTutorial,
  deleteTutorialById,
  fetchAllTutorials,
} from "../reduxs/actions/tutorialAction";
import { Button, Form, Container, Row } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Article() {
  const dispatch = useDispatch();
  const { tutorials } = useSelector((state) => state.tutorialReducer);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageURL, setImageURL] = useState("");

  useEffect(() => {
    dispatch(fetchAllTutorials());
  }, []);

  function onDeleteTutorialByid(id) {
    dispatch(deleteTutorialById(id));
  }

  async function onAddTutorial() {
    const tutorial = {
      title,
      description,
      thumbnail: imageURLs,
    };
    dispatch(addTutorial(tutorial));
  }

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [editID, seteditID] = useState(0);
  const [imageURLs, setImageURLs] = useState(
    "https://peacemakersnetwork.org/wp-content/uploads/2019/09/placeholder.jpg"
  );
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [submit, setsubmit] = useState(0);
  const [saveStatus, setsaveStatus] = useState(0);
  return (
    <>
      <div>
        <h1>Add Tutorial</h1>
      </div>
      <Container>
        <Row>
          <div className="col-8">
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  placeholder="Title"
                />

              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Author</Form.Label>
                <Form.Control
                  type="text"
                  value={author}
                  onChange={(e) => setAuthor(e.target.value)}
                  placeholder="Author"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Description</Form.Label>
                <Form.Control as="textarea" rows={3} value={description} onChange={(e) => setDescription(e.target.value)}  placeholder="Description" />
              </Form.Group>
              <Button variant="success" className="add" onClick={onAddTutorial}>
                Post
              </Button>{" "}
            </Form>
          </div>
          <div className="col-4">
            <img className="w-100" src={imageURLs} alt="" />
            <br></br>
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label="Choose Image"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURLs(url);
                  }}
                />
              </Form.Group>
            </Form>

          </div>
        </Row>
      </Container>

    </>
  );
}


