import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FacebookLogin from "react-facebook-login";
import {
  addAuthor,
  deleteAuthorById,
  fetchAllAuthors,

} from "../reduxs/actions/authorAction";
import { Col, Table, Button, Form, Container, Row } from "react-bootstrap";
import { editAuthorById, fetchAuthor, fetchAuthorById, postAuthor, } from '../services/authors.service'
export default function Author() {
  const dispatch = useDispatch();
  const { author } = useSelector((state) => state.authorReducer);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [editID, seteditID] = useState(0);
  const [imageURL, setImageURL] = useState("https://assets.atdw-online.com.au/images/69c811290c1ae47f88d42cc8b6fea39a.jpeg?rect=142");
  const [imageFile, setImageFile] = useState(null);
  const [authors, setAuthors] = useState([]);
  const [submit, setsubmit] = useState(0);
  const [saveStatus, setsaveStatus] = useState(0);
  const [uploadImage, setUploadImage] = useState("");


  useEffect(() => {
    dispatch(fetchAllAuthors());
  }, []);

  function onDeleteAuthorByid(id) {
    dispatch(deleteAuthorById(id));
  }

  async function onAddAuthor(e) {
    e.preventDefault();
    const author = {
      name,
      email,
      image: imageURL
    };
    dispatch(addAuthor(author));
    if (imageFile) {
      let url = await uploadImage(imageFile);
      author.image = url;
    }
    Promise.all([
      postAuthor(author).then((message) => alert(message)),
      setsubmit(submit + 1),
    ]);
    onFetch();
  };
  const onFetch = async () => {
    const result = await fetchAuthor();
    setAuthors(result);
    console.log(result);
  }

  const onSave = async (e) => {
    e.preventDefault();
    let author = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      author.image = url;
    }
    console.log(editID);
    editAuthorById(editID, author)
      .then((message) => alert(message))
      .then(setsubmit(submit + 1));
    setsaveStatus(0);
    onFetch();
    setsubmit(0);
  };

  const onEdit = async (id) => {
    const authorResult = await fetchAuthorById(id);
    setName(authorResult.name);
    setEmail(authorResult.email);
    setImageURL(authorResult.image);
    setsaveStatus(1);
    seteditID(id);
  };



  return (
    <>
      <div className="author">
        <h1>Author</h1>
      </div>
      <Container>
        <Row>
          <Col md={9}>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  placeholder="Author Name"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="text"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">

              </Form.Group>
              <Button variant="success" className="add" onClick={onAddAuthor}>
                Save
              </Button>{" "}
            </Form>
          </Col>
          <Col md={3}>
            <img className="w-100" src={imageURL} alt="" />
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label="Import Image"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Container>
      <br></br>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>NAME</th>
            <th>EMAIL</th>
            <th>IMAGE</th>
            <th>ACTION</th>
          </tr>
        </thead>
        <tbody>
          {
            author.map((item, index) => (
              <tr key={index}>
                <td>{item._id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td><img width="200px" src={item.image} /></td>
                <td>
                  <Button onClick={() => { onEdit(item._id) }} variant="primary">Edit</Button>{" "}
                  <Button onClick={() => onDeleteAuthorByid(item._id)} variant="warning">Delete</Button>
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    </>
  );
}
