import React, { useEffect, useState } from 'react'
import { Container, Form, Button, Table, } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import { addCategory, deleteCategoryById, fetchAllCategory } from '../reduxs/actions/categoryAction';
import { fetchCategoryById } from '../services/categorys.service';

export default function Category() {
    const dispatch = useDispatch();
    const { category } = useSelector((state) => state.categoryReducer);
    const [name, setName] = useState("");
    const [categorys, setCategorys] = useState([]);
    const [editID, seteditID] = useState(0);
    const [saveStatus, setsaveStatus] = useState(0);

    useEffect(() => {
        dispatch(fetchAllCategory());
    }, []);

    function onDeleteCategoryByid(id) {
        dispatch(deleteCategoryById(id));
      }

      const onEdit = async (id) => {
        const categoryResult = await fetchCategoryById(id);
        setName(categoryResult.name);
        setsaveStatus(1);
        seteditID(id);
      };
    

    async function onSaveCategory(e) {
        e.preventDefault();
        const category = {
          name  
        };
        dispatch(addCategory(category));
    }
    return (
        <>
            <div>
                <h1>Category</h1>

            </div>
            <Container>
                <Form>
                    <Form.Label>name</Form.Label>
                    <Form.Group className="mb-3 d-flex" controlId="exampleForm.ControlInput1">
                    <Form.Control className="me-2" type="text" value={name} onChange={(e) => setName(e.target.value)} placeholder="Input Category Name" />
                    <Button className="btnSave" variant="dark" className="add" onClick={onSaveCategory}>Save</Button>
                    </Form.Group>
                </Form>
            </Container>
            <br></br>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NAME</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        category.map((item, index) => (
                            <tr key={index}>
                              <td>{item._id}</td>
                              <td>{item.name}</td>
                                <td>
                                    <Button onClick={() => { onEdit(item._id) }} variant="primary">Edit</Button>{" "}
                                    <Button onClick={() => onDeleteCategoryByid(item._id)} variant="warning">Delete</Button>
                                </td>

                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </>
    )
}

    

