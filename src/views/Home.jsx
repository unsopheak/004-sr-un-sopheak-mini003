import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addTutorial,
  deleteTutorialById,
  fetchAllTutorials,
} from "../reduxs/actions/tutorialAction";
import {
  Button,
  Container,
  Row,
  Card,
  Col
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
export default function Home() {
  const dispatch = useDispatch();
  const { tutorials } = useSelector((state) => state.tutorialReducer);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageURL, setImageURL] = useState("");


  useEffect(() => {
    dispatch(fetchAllTutorials());
  }, []);

  function onDeleteTutorialByid(id) {
    dispatch(deleteTutorialById(id));
  }

  async function onAddTutorial() {
    const tutorial = {
      title,
      description,
      thumbnail: imageURL,
    };
    dispatch(addTutorial(tutorial));
  }
  return (
    <>

      <Container>
        <Row>
          <Col md={3}>
            <Card style={{ width: "15rem" }} className="profile">
              <Card.Img
                variant="top"
                src="https://article.innovadatabase.com/articleimgs/article_images/6371242945343885862012FIF%20main.jpg"
              />
              <Card.Body>
                <Card.Title>Please Login</Card.Title>
                <Button variant="primary">Login with Google</Button>
              </Card.Body>
            </Card>
          </Col>
          <Col md={9}>
            
            <h3 className="name-profile">Category</h3>
            <Button variant="light">metoo</Button> {""}
            <Button variant="light">sports</Button>

            <Row>

            {tutorials.map((item, index) => (

              
              <Col>
              <div>
                <div >
                  <div >
                    <Card style={{ width: "15rem" }} className="img">
                      <Card.Img
                        variant="top"
                        src={item.thumbnail}
                      />
                      <Card.Body>
                        <Card.Title>{item.title}</Card.Title>
                        <Card.Text className="text-line-3">
                          {item.description}
                        </Card.Text>
                        <Button variant="primary">View</Button>{" "}
                        <Button variant="warning">Edit</Button>{" "}
                        <Button
                          onClick={() => onDeleteTutorialByid(item._id)}
                          variant="danger"
                        >
                          Delete
                        </Button>
                      </Card.Body>

                    </Card>

                  </div>
                </div>
              </div>
              </Col>
            ))}

            </Row>
             
           

          </Col>
        </Row>
      </Container>


    </>
  );
}
