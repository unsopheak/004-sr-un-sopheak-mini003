import { API } from "./API";

export const fetch_all_categorys = async () => {
  try {
    const result = await API.get("/category");
    console.log("fetch_all_categorys", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("fetch_all_categorys", error);
  }
};

export const delete_category_by_id = async (id) => {
  try {
    const result = await API.delete("/category/" + id);
    console.log("delete_category_by_id", result);
    return result;
  } catch (error) {
    console.log("delete_category_by_id", error);
  }
};

export const add_category = async (category) => {
  try {
    const result = await API.post("/category", category);
    console.log("add_category:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("add_category error:", error);
  }
};

export const fetchCategory = async () => {
  try {
    const apiGetCategory = await API.get("category");
    return apiGetCategory.data.data;
  } catch (error) {
    console.log("fetchCategory Error:", error);
  }
};

export const editCategoryById = async (id, newCategory) => {
  let response = await API.put("category/" + id, newCategory);
  return response.data.message;
};
export const fetchCategoryById = async (id) => {
  let response = await API.get("category/" + id);
  return response.data.data;
};

export const postCategory = async (category) => {
  let response = await API.post("category", category);
  return response.data.message;
};

export const uploadImage = async (file) => {
  let formData = new FormData();
  formData.append("image", file);

  let response = await API.post("images", formData);
  return response.data.url;
};
