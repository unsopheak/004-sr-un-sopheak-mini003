import { API } from "./API";

export const fetch_all_authors = async () => {
  try {
    const result = await API.get("/author");
    console.log("fetch_all_authors", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("fetch_all_authors", error);
  }
};

export const delete_author_by_id = async (id) => {
  try {
    const result = await API.delete("/author/" + id);
    console.log("delete_author_by_id", result);
    return result;
  } catch (error) {
    console.log("delete_author_by_id", error);
  }
};

export const add_author = async (author) => {
  try {
    const result = await API.post("/author", author);
    console.log("add_author:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("add_author error:", error);
  }
};

export const fetchAuthor = async () => {
  try {
    const apiGetAuthor = await API.get("author");
    return apiGetAuthor.data.data;
  } catch (error) {
    console.log("fetchAuthor Error:", error);
  }
};

export const editAuthorById = async (id, newAuthor) => {
  let response = await API.put("author/" + id, newAuthor);
  return response.data.message;
};
export const fetchAuthorById = async (id) => {
  let response = await API.get("author/" + id);
  return response.data.data;
};

export const postAuthor = async (author) => {
  let response = await API.post("author", author);
  return response.data.message;
};

export const uploadImage = async (file) => {
  let formData = new FormData();
  formData.append("image", file);

  let response = await API.post("images", formData);
  return response.data.url;
};
