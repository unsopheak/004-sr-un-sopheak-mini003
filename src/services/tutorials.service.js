import { API } from "./API";

export const fetch_all_tutorials = async () => {
    try {
        const result = await API.get("/tutorials")
        console.log("fetch_all_tutorails", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("fetch_all_tutorails", error);
    }
}


export const delete_tutorial_by_id = async (id) => {
    try {
        const result = await API.delete("/tutorials/"+id)
        console.log("delete_tutorial_by_id", result);
        return result
    } catch (error) {
        console.log("delete_tutorial_by_id", error);
    }
}

export const add_tutorial = async(tutorial) =>{
    try {
        const result = await API.post("/tutorials", tutorial)
        console.log("add_tutorial:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("add_tutorial error:", error);
    }
}